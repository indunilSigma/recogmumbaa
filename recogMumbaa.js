const AWS = require('aws-sdk');
const rekognition = new AWS.Rekognition();

exports.handler = async (event) => {
    try {
        let data = await rekognition.detectFaces({
            Image: {
                S3Object: {
                    Bucket: "induuuu",
                    Name: "donald trump.jpg"
                }
            }
        }).promise();
console.log(data)
    } catch (err) {
        // error handling goes here
    };

    return { "message": "Successfully executed" };
};